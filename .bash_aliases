# Self-Defined commands
vid() {
    LINK=$1; EXTRA_PLAYER=$2; EXTRA_YTDL=$3
    [ "$LINK" ] || LINK=`xclip -o 2>/dev/null || xclip -o -selection clipboard`
    [[ "$LINK" == "-" ]] && read -p "URL: " LINK
    #Determine if we can use youtube-dl
    command -v youtube-dl > /dev/null
    if [ $? == "0" ]; then
        echo "Try to extract video URL..."
        LINK_=`youtube-dl $EXTRA_YTDL -g $LINK`
        if [ "$?" == 0 ]; then
            echo "Successfully extracted video URL."
            LINK=$LINK_
        else
            echo "Unable to extract video URL, maybe is already one."
        fi
    fi
    echo "Starting VLC."
    mpv --video-unscaled=downscale-big -fs $EXTRA_PLAYER $LINK
}

vids() {
    while read -p "Next: "; do
        echo $REPLY >> ~/Videos/vids.txt
        vid $REPLY $1 $2
    done
}

tempdir()
{ 
    local name=$1
    local tempdir=$(mktemp -p "" -d tempdir.XXXXXXXX)
    pushd ~
    ln -s $tempdir $([ "$name" ] && echo tempdir.$name)
    pushd $_
    echo $_ >> ~/.tempdirs
} 

_two_level_cd()
{
    local name=$1
    local dir=$2
    [ -d $dir/$name ] && cd $dir/$name || cd $dir/*/$name
}

dev() { _two_level_cd $1 ${DEV_DIR:-$HOME/Dokumente/dev}; }
proj() { _two_level_cd $1 ${PROJ_DIR:-$HOME/Dokumente/proj}; }

prod(){ cd ${PROD_DIR:-$HOME/Dokumente/prod}/$1; }

phablet-profile-sync()
{
    local go=$1
    [ "$go" == "go" ] && local no= || local no="-n"
    rsync -viatrm $no --include profiles.ini --include "*/" --include "*/key3.db" --include "*/cert8.db" --include "*/logins.json" --include "*/permissions.sqlite" --exclude '*' $HOME/.waterfox/ phablet@"$UBUNTU_TOUCH_HOST":profile
    [ "$no" ] && echo -e "\n\nDry run. Add \"go\" to do sync."
}

filebin(){
    curl https://p.elaon.de/api/v2.0.0/file/upload -F "apikey=$FILEBIN_API_KEY" -F "file=@-;filename=stdin"
}

gk_list(){
    # Requires hard tabs!
    python <<-EOT
	import gnomekeyring as gk
	for id in gk.list_item_ids_sync(None):
	    print("%s = %s" % (id, gk.item_get_info_sync(None, id).get_display_name()))
EOT
}

gk_get(){
    local id=$1
    python <<<"import gnomekeyring as gk; print(gk.item_get_info_sync(None, $id).get_secret())"
}
export -f gk_get

EXTR_HELPER_MOODLE_DL_PASSASK(){ gk_get $EXTR_HELPER_MOODLE_DL_PASS_GK_ID; }
export -f EXTR_HELPER_MOODLE_DL_PASSASK

todo(){
    case $1 in
        l|ls|list)
            less todo.txt
            ;;
        ""|show)
            head -n1 todo.txt
            ;;
        a|add)
            shift
            echo " - [ ] $@" >> todo.txt
            ;;
        dne|"done")
            head -n1 todo.txt >> todo.done && sed '1d' -i todo.txt && head -n1 todo.txt
            ;;
        e|edit)
            edit todo.txt
            ;;
        *)
            echo >&2 "Commands: show l|ls|list a|add dne|done e|edit"
            ;;
    esac
}

mdc(){ mkdir -p $1 && cd $1; }

..(){ cd ..; }
-(){ cd -; }

histclip(){ history 2 | head -n1 | cut -d" " -f5- | tr -d "\n" | xclip -i -selection clipboard;}

winReboot(){ sudo grub-reboot "Windows Boot Manager (auf /dev/sda2)";}

passgen(){ length=${1:-32}; cat /dev/urandom | tr -dc "a-zA-Z0-9\"\$%&/()[]{}=?*'+#.,:;_@-" | fold -w$length | head -n4; }

rs(){ gnome-terminal && exit; }
rsm(){ gnome-terminal --maximize && exit; }

_gitlab_json(){ echo $@; \gitlab $@ --json; }

mkuphp(){ echo '<?php move_uploaded_file($_FILES["up"]["tmp_name"], $_FILES["up"]["name"]);' > up.php; }

gl(){ fc -n -l -1 -1 | sed 's/^\s\+//'; }
sl(){ gl >> ${1:-command.sh}; }

# Aliases
alias rm="rm -i "
alias mv="mv -i "
alias cp="cp -i "
alias ll="ls -l "
alias la="ls -la "
alias rsync="rsync -tP " 
alias icurll="curl -IL "
alias p12g="vfs-rsync-upload "
alias gitlab="_gitlab_json "
alias urlencode="python -c 'import sys, urllib as ul; [sys.stdout.write(ul.quote_plus(l)) for l in sys.stdin]'"
alias urldecode="python -c 'import sys, urllib as ul; [sys.stdout.write(ul.unquote_plus(l)) for l in sys.stdin]'"
alias tabula="java -jar $TABULA "
alias debug_tabula="java -cp $TABULA technology.tabula.debug.Debug "
alias swagger-codegen="java -jar $SWAGGER_CODEGEN "
alias cmus='tmux new-session -A -D -s cmus "$(which cmus)" '

# Load local aliases
[ -f ~/.bash_aliases.local ] && . ~/.bash_aliases.local
