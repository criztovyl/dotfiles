# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

cd $PWD 2>/dev/null || cd

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
#HISTCONTROL=ignoreboth
HISTCONTROL=ignoredups:erasedups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=
HISTFILESIZE=
HISTTIMEFORMAT="%s "

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
#unset color_prompt 
unset force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    #alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Env
export GI_AUTHOR="Christoph \"criztovyl\" Schulz"

export MONTHDIFF_START_DEFAULT="2015-09"
export MONTHDIFF_END_DEFAULT_EXEC="date +%Y-%m"

export UBUNTU_TOUCH_HOST="ubuntu-phablet"
export EDITOR=vim

export GITLAB_API_ENDPOINT=https://gitlab.com/api/v3

export NPM_CONFIG_PREFIX=$HOME/.npm-global

export TOMCAT="$HOME/prod/tomee-web-7.0.3"

export PROD_DIR="$HOME/prod"

# External

sourceIfExists(){ [ -f $1 ] && . $1; }
LOCAL_OPT="$HOME/.local/opt"

for e in /usr/lib/git-core/git-sh-promp "$LOCAL_OPT/dlextract.sh/dlextract.sh" "$LOCAL_OPT/devpath.sh/devpath.sh" "$LOCAL_OPT/gpl-comment.sh/gpl-comment.sh" \
   "$LOCAL_OPT/password.sh/password.sh" "$HOME/.bashrc.private" "$HOME/.git_prompt.sh"
do
    sourceIfExists $e
done


get_sha(){ git rev-parse --short HEAD 2>/dev/null; }
export -f get_sha

shellparent(){ 
    local procfile="/proc/`cut -f4 -d' ' /proc/$$/stat`/cmdline"
    [ -f "$procfile" ] && cat $procfile | tr "\0" " " || echo
}
export -f shellparent

# Git Prompt

GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_DESCRIBE_STYLE="branch"
GIT_PS1_SHOWUPSTREAM="auto git"

export PROMPT_COMMAND='__git_ps1 "\n\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\n\[\033[00m\]" "\\\$$([ $SHLVL -gt 1 ] && echo $SHLVL)$([[ `shellparent` =~ "vim" ]] && echo "-vim:") " "[%s $(get_sha)]"'

# for root users:
#export PROMPT_COMMAND='__git_ps1 "\n\[\033[01;31m\]\u\[\\033[01;00m\]\[\033[01;32m\]@\h\[\033[00m\]:\[\033[01;34m\]\w\n\[\033[00m\]" "\\\$$([ $SHLVL -gt 1 ] && echo $SHLVL)$([[ `shellparent` =~ "vim" ]] && echo "-vim:") " "[%s $(get_sha)]"'

for path in "$HOME/.local/opt/bin" "$HOME/.local/bin" "$HOME/.gem/ruby/2.3.0/bin" "$HOME/.npm-global/bin"; do
    PATH=$path":"${PATH//$path:/} 
done; export PATH=$PATH

HOSTFILE="$HOME/.hosts"
shopt -s hostcomplete

set -o physical
set -o vi

# Clean up tempdirs
find $HOME -maxdepth 1 -name "tempdir.*" -xtype l -execdir rm -rf {} \;

# added by travis gem
[ -f /home/christoph/.travis/travis.sh ] && source /home/christoph/.travis/travis.sh

# JARs

_find_tabula_jar(){ find $1 -name "tabula*-jar-with-dependencies.jar" | sort --sort=version | head -n 1; }

if [ -d "$LOCAL_OPT/jars" ]; then
    export TABULA="$(_find_tabula_jar "$LOCAL_OPT/jars")"
fi

# Tabula source fallback
tabula_src="$HOME/src/tabula-java/target"
[ -z "$TABULA" ] && [ -d "$tabula_src" ] && export TABULA="$(_find_tabula_jar "$tabula_src")"

# swagger-codegen
export SWAGGER_CODEGEN="$LOCAL_OPT/lib/jars/swagger-codegen-cli.jar"

alias dquilt="quilt --quiltrc=${HOME}/.quiltrc-dpkg"
complete -F _quilt_completion -o filenames dquilt

which rbenv >/dev/null && eval "$(rbenv init -)"
