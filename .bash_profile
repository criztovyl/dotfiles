# Perl
PATH="$HOME/.local/bin${PATH:+:${PATH}}"
PERL5LIB="$HOME/.local/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
PERL_LOCAL_LIB_ROOT="$HOME/.local${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
PERL_MB_OPT="--install_base \"$HOME/.local\""
PERL_MM_OPT="INSTALL_BASE=$HOME/.local"

GOPATH=$HOME/go
PATH="$HOME/.rbenv/bin:$HOME/.cargo/bin:$PATH:${GOPATH//://bin:}/bin:/usr/local/go/bin"

export PATH GOPATH PERL5LIB PERL_LOCAL_LIB_ROOT PERL_MB_OPT PERL_MM_OPT

if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi
