#!/usr/bin/env bash
shopt -s dotglob extglob

[ `pwd` == "$HOME" ] && { echo >&2 "Only run from cloned directory itself!"; exit 1; }

for file in !(.|..|install.sh|.git|.gitignore|.gitmodules|util); do

    echo "Installing $file..."

    [ -f "$HOME/$file" ] && [ ! -L "$HOME/$file" ] && mv $HOME/$file $HOME/$file.$(date +%Y-%m-%d-%s).bak
    ln -s -t $HOME .dotfiles/$file -f

done
