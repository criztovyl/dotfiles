" Pathogen
execute pathogen#infect()

" Minimal
syntax on
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab
set number
" set showmatch " matching bracet
set showmode
set showcmd
set ruler
set laststatus=2
set ignorecase
set smartcase
set smartindent
set smarttab
set lazyredraw
set viminfo='20,\"500
set hidden
set hlsearch
set incsearch

" break long lines
set textwidth=80
set linebreak

" No backup
set nobackup
set nowb
set noswapfile

" http://stackoverflow.com/questions/5700389/using-vims-persistent-undo
set undofile                " Save undo's after file closes
set undodir=$HOME/.vim/undo " where to save undo histories
set undolevels=1000         " How many undos
set undoreload=10000        " number of lines to save for undo

let mapleader=","
let g:mapleader=","

imap jjj <esc>jjj
imap kkk <esc>kkk
imap lll <esc>lll
imap hhh <esc>hhh

" Arrow keys are uncool!
noremap  <Up> <NOP>
noremap  <Down> <NOP>
noremap  <Left> <NOP>
noremap  <Right> <NOP>
noremap  <Home> <NOP>
noremap  <End> <NOP>
noremap  <PageUp> <NOP>
noremap  <PageDown> <NOP>

inoremap   <Up>     <NOP>
inoremap   <Down>   <NOP>
inoremap   <Left>   <NOP>
inoremap   <Right>  <NOP>
inoremap   <Home>   <NOP>
inoremap   <End>    <NOP>
inoremap  <PageUp> <NOP>
inoremap  <PageDown> <NOP>
"
" Move a line of text using ALT+[jk] or Command+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
map <leader>r :set invrnu<cr>
map <leader><cr> :noh<cr>

" https://github.com/amix/vimrc/blob/master/vimrcs/basic.vim#L213
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

colorscheme torte

" Always jump to last known position
autocmd BufReadPost * 
    \ if line("'\"") > 0 && line("'\"") <= line("$") | 
    \   exe "normal g`\"" | 
    \ endif 

" https://github.com/amix/vimrc/blob/master/vimrcs/basic.vim#L382
function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ag \"" . l:pattern . "\" " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

" https://github.com/amix/vimrc/blob/master/vimrcs/basic.vim#L409
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
    let l:currentBufNum = bufnr("%")
    let l:alternateBufNum = bufnr("#")

    if buflisted(l:alternateBufNum)
        buffer #
    else
        bnext
    endif

    if bufnr("%") == l:currentBufNum
        new
    endif

    if buflisted(l:currentBufNum)
        execute("bdelete! ".l:currentBufNum)
    endif
endfunction

" NERDTree
" Auto-open when starting vim w/o files
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Close when is last window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Mapping
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeWinPos = "right"

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" let g:syntastic_c_config_file = '.config_c'
let g:syntastic_c_no_include_search = 1

let g:syntastic_java_checkers=['javac']
let g:syntastic_java_javac_config_file_enabled = 1

" Only do this part when compiled with support for autocommands.
if has("autocmd")
    autocmd Filetype java setlocal omnifunc=javacomplete#Complete
endif 

" https://gist.github.com/MSch/1062382
au BufNewFile,BufRead *.jst set syntax=jst

" http://vim.wikia.com/wiki/Modeline_magic
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
        \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

" http://vim.wikia.com/wiki/Highlight_unwanted_spaces
:set list listchars=tab:»·,trail:·

" jekyll frontmatter
au BufNewFile,BufRead,BufWrite *.md syntax match Comment /\%^---\_.\{-}---$/
